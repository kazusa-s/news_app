require 'rails_helper'

RSpec.describe User, type: :model do
  it 'ユーザーネーム、メールアドレス、パスワードがあれば有効な状態であること' do
    user = User.new(
      username: "Kazusa",
      email:      "test@example.com",
      password:   "testtest"
    )
    expect(user).to be_valid
  end

  it '名前が空欄の場合、エラーになること' do
    user = User.new(
      username: "",
      email:      "test@example.com",
      password:   "testtest"
    )
    expect(user).to_not be_valid
    # expect(user.errors[:username]).to include("名前を入力してください")
  end

  it 'メールアドレスが空欄の場合、エラーになること' do
    user = User.new(
      username: "Kazusa",
      email:      "",
      password:   "testtest"
    )
    expect(user).to_not be_valid
    # expect(user.errors[:email]).to include("メールアドレスを入力してください")
  end

  it 'メールアドレスが不正の値の場合、エラーになること' do
    user = User.new(
      username: "Kazusa",
      email:      "aaaaaaaaa",
      password:   "testtest"
    )
    expect(user).to_not be_valid
    # expect(user.errors[:email]).to include("正しいメールアドレスを入力してください")
  end
  it 'パスワードが空欄の場合、エラーになること' do
    user = User.new(
      username: "Kazusa",
      email:      "test@example.com",
      password:   ""
    )
    expect(user).to_not be_valid
    # expect(user.errors[:password]).to include("パスワードを入力してください")
  end

  it 'パスワードが5文字以下の場合、エラーになること' do
    user = User.new(
      username: "Kazusa",
      email:      "test@example.com",
      password:   "test"
    )
    expect(user).to_not be_valid
    # expect(user.errors[:password]).to include("パスワードは6文字以上で入力してください")
  end
end
