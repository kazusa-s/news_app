Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/curations/top'
  put '/curations', to: 'curations#fetch_news'
  resources :curations do
    resources :comments
  end

  root to: 'curations#index'


end
