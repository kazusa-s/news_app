class Curation < ApplicationRecord
  has_many :comments
  validates :url, uniqueness: true

end
