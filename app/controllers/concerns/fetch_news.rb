module FetchNews
  extend ActiveSupport::Concern

  def fetch_news
  # -------------- Google_api --------------
      @url = 'https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=' + ENV["NEWS_API_KEY"]
      @req = open(@url)
      @response_body = @req.read
      @obj = JSON.parse(@response_body)

      # @articles = []
      # @obj["articles"].each do |obj|
      # @articles  << obj["url"]
      # end
      # @topheader = Google_api.get_top_header

      #データベースに登録
      @obj["articles"].each do |content|
        @curation1 = Curation.new(
          source_id: 1,
          title: content["title"],
          description: content["description"],
          url: content["url"],
          urlToImage: content["urlToImage"]
        )
        @curation1.save
      end
  # --------------end Google_api end--------------

  # -------------- api_gem --------------
      @newsapi = News.new(ENV["NEWS_API_KEY"])
      @top_headlines = @newsapi.get_top_headlines(sources: "bbc-news",language: "en")

      #データベースに登録
      @top_headlines.each do |content|
        @curation2 = Curation.new(
          source_id: 2,
          title: content.title,
          description: content.description,
          url: content.url,
          urlToImage: content.urlToImage
        )
        @curation2.save
      end
  # --------------end api_gem end--------------

  # -------------- nokogiri --------------

      #ソースページ＋文字コード取得
      url = "https://www.nikkei.com/technology/"
      @charset = nil
      html = open(url) do |f|
        @charset = f.charset
        f.read
      end

      #htmlの解析
      @doc = Nokogiri::HTML.parse(html,nil,@charset)
      @title = ""
      @image = ""
      @description = ""
      @url = ""
      @content = []

      #HTML要素の切り出し
      @doc.xpath('//div[@class="m-miM09"]').each do |content|
        @image = content.css('img').attribute('src').value
        @title =  content.css('.m-miM09_title').inner_text
        @description = content.css('p').inner_text
        @url = "https://www.nikkei.com" + content.css('a').attribute('href').value

        #データベースに登録
        @curation3 = Curation.new(
          source_id: 3,
          title: @title,
          description: @description,
          url: @url,
          urlToImage: @image
        )
        @curation3.save

        #データベースを使わない場合は以下のようなデータ構造で格納 > @dataが１つの記事になる
        # @data = {"urlToImage" => @image, "title" => @title, "description" => @description, "url" => @url}
        # @content << @data

       end
       # --------------end nokogiri end--------------
       redirect_to curations_path
end

end
