class CommentsController < ApplicationController

  before_action :authenticate_user!

  def create
    # p current_user

    @article = Curation.find(params[:curation_id])
    @comment = @article.comments.create(comment_params)

    @user_id = @comment.update(
      user_id: current_user.id
    )


    redirect_to curation_path(@article)
  end

  private
  def comment_params
    params.require(:comment).permit(:body)
  end

end
