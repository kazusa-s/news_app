require 'open-uri'
require 'news-api'
require 'nokogiri'

class CurationsController < ApplicationController
  before_action :news_index, only: [:index]
  after_action :news_index, only: [:fetch_news]

  include FetchNews  #fetch_newsメソッドをモジュール化

  def news_index  #データベースからニュースを検索
    @curation1 = Curation.where(source_id:1).page(params[:curation1]).per(5)
    @curation2 = Curation.where(source_id:2).page(params[:curation2]).per(5)
    @curation3 = Curation.where(source_id:3).page(params[:curation3]).per(5)
    @tab_flg = params[:tab_flg]
  end

  def index
  end



  def show #ニュースを個別で表示
    @article = Curation.find(params[:id])
  end

  def top
  end
end
