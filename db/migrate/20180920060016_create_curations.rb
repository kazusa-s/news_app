class CreateCurations < ActiveRecord::Migration[5.2]
  def change
    create_table :curations do |t|
      t.integer :source_id
      t.string :title
      t.text :description
      t.text :url
      t.text :urlToImage

      t.timestamps
    end
  end
end
